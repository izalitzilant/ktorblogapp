package com.example

import com.example.database.DAOFacadeImpl
import com.example.database.initDatabase
import io.ktor.server.application.*
import com.example.plugins.*
import io.ktor.server.routing.*

fun main(args: Array<String>): Unit =
    io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // application.conf references the main function. This annotation prevents the IDE from marking it as unused.
fun Application.module() {   // по умолчанию здесь шаблонный код,
    initDatabase(environment.config) //но это не помешает рассмотреть
    val dbfacade = DAOFacadeImpl()   //технологию
    BooksLogicApiRouting(dbfacade)
}


