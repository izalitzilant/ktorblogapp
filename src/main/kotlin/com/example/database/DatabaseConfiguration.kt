package com.example.database

import com.example.models.Book
import com.example.models.Books
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.server.config.*
import io.ktor.server.util.*
import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*

fun initDatabase(config: ApplicationConfig) {
    val driverClassName = config.property("storage.driver").getString()
    val jdbcURL = config.property("storage.jdbcURL").getString()
    val dbUsername: String = config.property("storage.user").getString()
    val dbPassword = config.property("storage.password").getString()
    val dataSource = createHikariDataSource(
        url = jdbcURL,
        driver = driverClassName,
        user = dbUsername,
        dbPassword = dbPassword
    )

    Database.connect(
        dataSource
    )

    transaction {
        addLogger(StdOutSqlLogger)
        SchemaUtils.create(Books)

        Books.insert {
            it[author] = "Tolkien"
            it[title] = "The Lord of Rings"
        }

        Books.insert {
            it[author] = "Bruce Eckel, Svetlana Isakova"
            it[title] = "ATOMIC KOTLIN"
        }

        Books.insert {
            it[author] = "Stephen Cleary"
            it[title] = "Concurrency in C# Cookbook"
        }

    }
}

private fun createHikariDataSource(
    url: String,
    driver: String,
    user: String,
    dbPassword: String
) = HikariDataSource(HikariConfig().apply {
    username = user
    password = dbPassword
    jdbcUrl = url
    maximumPoolSize = 3
    isAutoCommit = false
    transactionIsolation = "TRANSACTION_REPEATABLE_READ"
    validate()
})

interface DAOFacade {
    suspend fun getBookById(id: Int): Book?
    suspend fun addNewBook(author: String, title: String)
}

class DAOFacadeImpl : DAOFacade {
    override suspend fun getBookById(id: Int): Book? =
        dbQuery { Books.select { Books.id.eq(id) }.map { Book.fromRow(it) }.single() }

    override suspend fun addNewBook(_author: String, _title: String) {
        dbQuery {
            Books.insert {
                it[author] = _author
                it[title] = _title
            }
        }
    }
}

private suspend fun <T> dbQuery(block: () -> T): T = newSuspendedTransaction(Dispatchers.IO) { block() }