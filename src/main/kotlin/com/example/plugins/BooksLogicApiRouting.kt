package com.example.plugins

import com.example.database.DAOFacade
import com.example.database.DAOFacadeImpl
import com.example.models.Books
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Application.BooksLogicApiRouting(daoFacade: DAOFacadeImpl) {
    routing {
        route("/books") {
            get("/get?{id?}") {
                val nfi = "not found id"
                val id = call.parameters["id"] ?: nfi
                if (id != nfi) {
                    val result = daoFacade.getBookById(id.toInt()) ?: "Not found"
                    call.respondText(result.toString())
                }
            }
            post("/create") {
                val formParameters = call.receiveParameters()
                daoFacade.addNewBook(

                    _author = formParameters["author"]?: "|No author|",
                    _title = formParameters["title"]?: "|No title|"
                )
            }
        }
    }
}