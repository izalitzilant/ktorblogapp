package com.example.models

import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.Table.Dual.varchar

object Books : IntIdTable() {
    val title: Column<String> = varchar("title", 50)
    val author: Column<String> = varchar("author", 50)
}

data class Book(
    val id: Int,
    val title: String,
    val author: String
) {
    companion object {
        fun fromRow(resultRow: ResultRow) = Book(
            id = resultRow[Books.id].value,
            title = resultRow[Books.title],
            author = resultRow[Books.author]
        )
    }

    override fun toString(): String {
        return "$id\n$title\n$author\n"
    }
}